Vue.component("app-saludo", {
    "template": `<h2> Hola, me llamo componente </h2>`
});

Vue.component("fondo-oscuro", {
    "template": `<div>
        <h1>
            EL FONDO NEGRO ES MEJOR!
        </h1>
        <h2>
            Y no hay más que hablar C:
        </h2>
    </div>`
});

new Vue({
    el: "#app-section",
    template: `
    <div>
        <h2> Jelouda </h2>
        <app-saludo> </app-saludo> 
        <fondo-oscuro> </fondo-oscuro>
    </div>
    `
});


new Vue({
    el: "#app-section-2",
    template: `<app-saludo> </app-saludo>`
});

new Vue({
    el: "#app-section-3",
    template: `<fondo-oscuro> </fondo-oscuro>`
});