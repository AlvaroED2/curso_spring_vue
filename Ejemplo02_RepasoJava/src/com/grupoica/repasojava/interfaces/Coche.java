package com.grupoica.repasojava.interfaces;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Coche extends Vehiculo implements Motorizable {

	float combustible;

	public Coche(String marca, float peso, float combustible) {
		super(marca, peso);
		this.combustible = combustible;
		// TODO Auto-generated constructor stub
	}

	public float getCombustible() {
		return combustible;
	}

	public void setCombustible(float combustible) {
		this.combustible = combustible;
	}

	@Override
	public void acelerar() {
		// super.acelerar();
		System.out.println(
				"SOY " + marca + " VIRGOLINIIIII CON " + getCombustible() + "L\nFIIIIIIIIIIIIIIIIIIIIIIIIUMM");
		
	}

	@Override
	public void desplazarse(float distancia) {
		 
		System.out.println(marca + " rueda " + distancia +" metros");
		
	}

	@Override
	public void encender() {
		
		//Mensajes personalizados de encendido de coche
		System.out.println("COCHE " + marca + " ENCENDIDO Y LISTO PARA DESPLAZAMIENTO");
		
		
	}

}
