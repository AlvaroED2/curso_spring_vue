package com.grupoica.repasojava.interfaces;

public class Perro implements Animal {

	private String nombre;
	public Perro(String nombre) {
		this.nombre = nombre;
	}
	
	

	public String getNombre() {
		return nombre;
	}



	public void setNombre(String nombre) {
		this.nombre = nombre;
	}



	@Override
	public void alimentarse(String comida) {
		
		
		System.out.println(nombre+ " se alimenta y te da las gracias");

	}

}
