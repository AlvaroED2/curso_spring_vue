package com.grupoica.repasojava.interfaces;

public abstract class Vehiculo {

	String marca;
	float peso;
	
	
	
	public Vehiculo(String marca, float peso) {
		this.marca = marca;
		this.peso = peso;
	}
	
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public float getPeso() {
		return peso;
	}
	public void setPeso(float peso) {
		this.peso = peso;
	}
	
	public void acelerar() {
		System.out.println( getClass().getSimpleName()+ " marca: " + getMarca()+ " Acelerando...\nFIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIUUUUUUUUUUUUUUUUUUUUUUUUUUUM");
	}
	
	public abstract void desplazarse (float distancia);
	
}
