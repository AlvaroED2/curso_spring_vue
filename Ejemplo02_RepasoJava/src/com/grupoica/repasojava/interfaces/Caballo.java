package com.grupoica.repasojava.interfaces;

public class Caballo extends Vehiculo implements Animal {

	int dientes;

	public Caballo(String marca, float peso, int dientes) {
		super(marca, peso);
		this.dientes = dientes;
	}

	public int getDientes() {
		return dientes;
	}

	public void setDientes(int dientes) {
		this.dientes = dientes;
	}

	@Override
	public void acelerar() {
		
		System.out.println("Soy un caballito " + marca + " mu bonico que acelera");
		
	}

	@Override
	public void desplazarse(float distancia) {
		System.out.println(marca + " trota " + distancia +" metros");
		
	}

	@Override
	public void alimentarse(String comida) {
		
		System.out.println("Caballito " + marca + " se alimenta de " + comida);
		
	}

}
