package com.grupoica.repasojava.interfaces;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.Map;
import java.util.Scanner;

public class ProbandoVehiculos {

	public static void probar() {
		
		//Inicializamos los coches
		Coche miCoche = new Coche("Kia", 1500, 50f);
		Coche segundoCoche = new Coche("Lamborghini",1400,60f);
		Coche cocheFinesSemana = new Coche("Audi",1600,80f);
		
		//Inicializamos los patinetes
		Patinete patin = new Patinete(30f);
		Patinete patin2 = new Patinete (40f);
		
		//Creamos array garage para SOLO motorizables
		ArrayList<Motorizable> garage = new ArrayList<>();
		
		//A�adimos coches
		garage.add(miCoche);
		garage.add(segundoCoche);
		garage.add(cocheFinesSemana);
		
		//A�adimos patinetes
		garage.add(patin);
		garage.add(patin2);
		
		for(Motorizable veh: garage) {
			
			
			
			veh.encender();
			
			if(veh instanceof Vehiculo) {
				
				Vehiculo vehiculo = (Vehiculo) veh;
				
				//Mensaje personalizado que pide al usuario cuanto desea desplazarse
				System.out.println("CUANTOS METROS DESEA DESPLAZARSE");
				Scanner sc = new Scanner(System.in);
				
				try {
				float distancia = sc.nextFloat();
				
				vehiculo.desplazarse(distancia);
				}catch(InputMismatchException e) {
					System.out.println("DEBES INDICAR LA VELOCIDAD EN NUMERO");
				}
			}
			
			
		}
		
		//Creamos los perros
		Perro perrito1 = new Perro("Kira");
		Perro perrito2 = new Perro("Blue");
		Perro perrito3 = new Perro("Thor hijo de Odin");
		
		//Creamos los caballos
		Caballo caballo1 = new Caballo("Unicornio",400,30);
		Caballo caballo2 = new Caballo("Chucho",250,50);
		Caballo caballo3 = new Caballo("Poni", 300,45);
		
		
		//Creamos el arraylist de los animales
		ArrayList<Animal> animales = new ArrayList<>();
		animales.add(perrito1);
		animales.add(perrito2);
		animales.add(perrito3);

		animales.add(caballo1);
		animales.add(caballo2);
		animales.add(caballo3);
		
		//
		//
		//Granja con hashmap
		HashMap<String, Animal> granja = new HashMap<>();
		
		granja.put("Perro1",perrito1);
		granja.put("Caballo1",caballo1);
		granja.put("Perro2",perrito2);
		granja.put("Caballo2",caballo2);
		granja.put("Perro3",perrito3);
		granja.put("Caballo3",caballo3);
		
		for(Map.Entry<String, Animal> animal : granja.entrySet()) {
			animal.getValue().alimentarse("Pienso");
		}
		
		for(Animal animalicos: animales) {
			System.out.println("----------------------");
			animalicos.alimentarse("Pienso");
			
		}
		
		
	}
	
	

}
