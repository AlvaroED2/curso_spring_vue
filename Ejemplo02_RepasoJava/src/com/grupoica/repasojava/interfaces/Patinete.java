package com.grupoica.repasojava.interfaces;

public class Patinete implements Motorizable {

	private float bateria;
	
	public Patinete(float bateria) {
		this.bateria = bateria;
	}

	public float getBateria() {
		return bateria;
	}

	public void setBateria(float bateria) {
		this.bateria = bateria;
	}
	
	

	@Override
	public void encender() {
		
		System.out.println("BATERIA DE: " + bateria + "W ENCENDIDA AL "+
				Math.floor(Math.random()*100+1) +"% DE CARGA");
		
	}

}
