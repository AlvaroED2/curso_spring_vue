package com.grupoica.repasojava;

public class EjemploLambda {

	// Una interfaz es funcion cuando SOLO TIENE UN M�TODO
	interface NuestraFuncion {
		float operacion(float x, float y);
	}

	public static void ejecutarLamda() {
		
		float arr[] = {3,4,65,7,8};
		float arr2[] = {5,7,8,3,4};
		
		float arrRes []=operarArrays(arr, arr2, EjemploLambda::sumar);
		
		
		float arrResta []=operarArrays(arr, arr2, EjemploLambda::restar);
		float arrDiv [] = operarArrays(arr, arr2, (float x,float y)->{
			return x/ y;
		});
		
		
		float mult [] = operarArrays(arr,arr2,(float x, float y)->{
			System.out.println("Multiplicando: " + x + " * " + y +" = " + x*y);
			return x * y;
		});
		
		for (int i = 0; i < arrRes.length; i++) {
			System.out.print(arrRes[i] + "|");
		}
	}

	public static float sumar(float x, float y) {
		return x + y;
	}

	public static float restar(float x, float y) {
		return x - y;
	}

	public static float[] operarArrays(float[] arr1, float[] arr2, NuestraFuncion funCallback) {
		float[] arrResultado = new float[arr1.length];

		for (int i = 0; i < arr1.length && i < arr2.length; i++) {
			arrResultado[i] = funCallback.operacion(arr1[i], arr2[i]);
		}

		return arrResultado;
	}

}
