package com.grupoica.repasojava;

import java.util.HashMap;
import java.util.Scanner;

import com.grupoica.repasojava.modelo.Usuario;

public class EjemploHashMap {

	static HashMap<String, Usuario> diccUsuarios;
	public static void probandoHashMap() {
		
		diccUsuarios = new HashMap<>();
		diccUsuarios.put("Luis",new Usuario("Luis",30));
		diccUsuarios.put("Mark",new Usuario("Mark",17));
		diccUsuarios.put("Oliver",new Usuario("Oliver",15));
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Introduzca el usuario:");
		String nombre = sc.nextLine();
		System.out.println("EL usuario es:" + diccUsuarios.get(nombre));
	}
	
}
