package com.grupoica.repasojava.modelo;

import java.util.ArrayList;
import java.util.Iterator;

/*
 * Clase que se encargar� de las operaciones C.R.U.D.
 * Create Read Update Delete (Alta,buscar ,modificar y borrar)
 */
public class GestionUsuarios {

	// Forma generica (Todos los elementos son del mismo o de algun tipo heredero
	private ArrayList<Usuario> listaUsuariosU;

	public GestionUsuarios() {
		super();
		this.listaUsuariosU = new ArrayList<Usuario>();

	}

	public void listarUsuarios() {
		for (int i = 0; i < listaUsuariosU.size(); i++) {
			System.out.println(listaUsuariosU.get(i).toString());

		}
	}

	public void anadirUsu(Usuario usu) {
		this.listaUsuariosU.add(usu);
	}

	public void mostrarUsuarioNombre(String nombre) {

		double doble = 10.43434;
		int entero = (int) doble;
		for (Usuario usu : listaUsuariosU) {
			if (usu.getNombre().equals(nombre)) {
				System.out.println("Nombre: " + usu.getNombre());
			}
		}

	}

	public void modificar(int edad, String nombre, String nombreBusqueda) {

		for (Usuario usu : listaUsuariosU) {
			if (usu.getNombre().equals(nombreBusqueda)) {
				usu.setEdad(edad);
				usu.setNombre(nombre);
			}
		}
	}

	public void modificar(int edad, String nombreBusqueda) {

		for (Usuario usu : listaUsuariosU) {
			if (usu.getNombre().equals(nombreBusqueda)) {
				usu.setEdad(edad);
			}
		}
	}

	public void modificar(String nombre, String nombreBusqueda) {

		for (Usuario usu : listaUsuariosU) {
			if (usu.getNombre().equals(nombreBusqueda)) {
				usu.setNombre(nombre);
			}
		}
	}

	public void filtroEdad(int edad) {

		filtroEdad(edad, edad);

	}

	public void filtroEdad(int edadMinima, int edadMaxima) {

		boolean encontrado = false;
		for (Usuario usu : listaUsuariosU) {

			if (usu.getEdad() >= edadMinima && usu.getEdad() <= edadMaxima) {
				encontrado = true;

				System.out.println(usu.toString());
			}
		}

		if (!encontrado) {
			System.out.println("No se ha encontrado ning�n usuario");
		}
	}

	public void eliminarUsuario(String nombre) {

		Iterator<Usuario> recorrer = listaUsuariosU.iterator();

		while (recorrer.hasNext()) {
			Usuario usu = recorrer.next();
			if (usu.getNombre().equals(nombre)) {
				recorrer.remove();
			}

		}

	}

	public void eliminarTodo() {

		listaUsuariosU.clear();

	}

}
