package com.grupoica.repasojava;

import com.grupoica.repasojava.interfaces.ProbandoVehiculos;
import com.grupoica.repasojava.modelo.GestionUsuarios;
import com.grupoica.repasojava.modelo.Loco;
import com.grupoica.repasojava.modelo.Usuario;

public class ProgramaMain {

	/*
	 * P.O.O La unidad basica de almacenamiento son los tipos primitivos y los
	 * objetos que est�n basados en clases. Las clases son el molde, plantilla,
	 * estructura que indican como ser�n todos los objetos instanciados a partir de
	 * ella: Sus variables miembro(campos,atributos,propiedades...) y sus m�todos
	 * (funciones propias) -Herencia: La capacidad de las clases para heredad los
	 * metodos y variables miembro unas de otras. Usando la palabra extends.
	 * -Encapsulaci�n : Capacidad para limitar el acceso a variables miembro y
	 * metodos -Polimorfismo: La capacidad de los objetos de obtener la forma de su
	 * clase o la de cualquiera de sus ancestros(padres,abuelos,...)
	 */

	public static void main(String[] args) {

//		GestionUsuarios gestor = new GestionUsuarios();
//		gestor.anadirUsu(new Usuario("Mengano", 20));
//		gestor.anadirUsu(new Usuario("Cara", 39));
//		Loco loco = new Loco(true, "Joker", 24);
//		gestor.anadirUsu(loco);
//		gestor.filtroEdad(24);
//		gestor.filtroEdad(20, 24);
//		gestor.modificar(30, "Joker");
//		gestor.modificar("Jokero", "Joker");
//		gestor.listarUsuarios();
//		gestor.eliminarUsuario("Jokero");
//		gestor.listarUsuarios();
//		gestor.eliminarTodo();
//		gestor.listarUsuarios();
		
//		EjemploHashMap.probandoHashMap();
//		ProbandoVehiculos.probar();
		EjemploLambda.ejecutarLamda();
		

	}

}
