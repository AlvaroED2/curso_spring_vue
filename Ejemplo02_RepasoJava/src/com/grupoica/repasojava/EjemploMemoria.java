package com.grupoica.repasojava;

import com.grupoica.repasojava.modelo.Usuario;

public class EjemploMemoria {
	
	static int x = 10;
	
	public static void pruebaPasoPorValor() {
		
		boolean y = true; 
		String z = "Texto al declarar";
		funcionCualquiera(x, y, z);
		System.out.println("Xx = " + x + " Y= " + y + " Z = " + z);
	}
	
	private static void funcionCualquiera(int x,boolean y, String z) {
	
		System.out.println("X = " + x + " Y= " + y + " Z = " + z);
		x = 200;
		y = false;
		z = "new text on function";
		System.out.println("X = " + x + " Y= " + y + " Z = " + z);
	}
	
	public static void pruebaPasoPorReferencia() {
		Usuario user = new Usuario("Markus",40);
		int array[] = new int [3];
		array[0] = 10;
		array[1]= 20;
		array[2] = 30;
		System.out.println("Nombre " + user.getNombre() + "\nElemento 0 " + array [0]);
		
		cambioDatos(array,user);
		
		System.out.println("Nombre " + user.getNombre() + "\nElemento 0 " + array [0]);
	}
	
	public static void cambioDatos(int [] array, Usuario user) {
		
		array [0] = 22;
		user.setNombre("Manolo");
		
		int [] array2 = array;
		array2[0] = 333;
		System.out.println("Elemento 0 " + array[0]);
		
	}

}
