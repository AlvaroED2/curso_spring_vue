package com.grupoica.appspring.modelo.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.grupoica.appspring.modelo.daos.HeroesDAO;
import com.grupoica.appspring.modelo.entidades.Heroe;

import java.util.List;

@RestController
@RequestMapping("/api/heroes")
@CrossOrigin
public class ControladorHeroes {

	@Autowired
	HeroesDAO heroesDao;
	
	@RequestMapping(method = RequestMethod.GET,value = "/prueba")
	public Heroe leerHeroe() {
		System.out.println("Hola");
		Heroe h = new Heroe();
		h.setNombre("Hulk");
		return h;
		
	}
	
	@PostMapping
	public Heroe guardarHeroe(@RequestBody Heroe nuevoHeroe) {
		System.out.println("Heroe recibido >>>>>>>>>>" + nuevoHeroe.getNombre());
		return heroesDao.save(nuevoHeroe);
	}
	
	@PutMapping
	public Heroe actualizarHeroe(@RequestBody Heroe nuevoHeroe) {
		if(nuevoHeroe.getId() > 0) {
			System.out.println("Heroe recibido >>>>>>>>>>" + nuevoHeroe.getNombre());
			return heroesDao.save(nuevoHeroe);
		}else {
			System.out.println("Heroe recibido sin ID >>>>>>>>>>" + nuevoHeroe.getNombre());
			return null;
		}
		
		
	}
	
	@DeleteMapping
	public void borrarHeroe(@RequestBody Heroe nuevoHeroe) {
		if(nuevoHeroe.getId() > 0) {
			System.out.println("Heroe eliminado >>>>>>>>>>" + nuevoHeroe.getNombre());
			heroesDao.delete(nuevoHeroe);
		}else {
			System.out.println("Heroe recibido sin ID >>>>>>>>>>" + nuevoHeroe.getNombre());
		}
		
		
	}
	
	@RequestMapping(value="{id}",method = RequestMethod.DELETE)
	public void borrarHeroe(@PathVariable int id) {
		if(id > 0) {
			System.out.println("Heroe eliminado >>>>>>>>>>" + id);
			heroesDao.deleteById(id);
		}else {
			System.out.println("Heroe recibido sin ID >>>>>>>>>>");
		}
		
		
	}
	
	@RequestMapping(value="{id}",method = RequestMethod.GET)
	public Heroe getUno(@PathVariable int id) {
		if(id > 0) {
			System.out.println("Heroe eliminado >>>>>>>>>>" + id);
			return heroesDao.findById(id).get();
		}else {
			System.out.println("Heroe recibido sin ID >>>>>>>>>>");
			return null;
		}
	}
	
	@RequestMapping(method = RequestMethod.GET)
	public List<Heroe> getAll() {
		return heroesDao.findAll();
	}
	
	@RequestMapping("/hola")
	public String bienvenida() {
		return "hola";
	}
	
	@RequestMapping("/error")
	public String error() {
		return "Error";
	}
}
