//Otra forma de crear objetos
//JSON: Javascript object notation

let objetoVacio = {}; //== New Object()
let array = [] // ==new Array()

/*
let formaPago = {
    "modo": "Tarjeta",
    "comision": 2,
    "activa": true,
    "preparacion": null,
    "clientes": [
        "Santander",
        "Sabadell",
        "BBVA", [1, 23, 55]
    ],
    "configuracion": {
        "conexion": "ssl",
        "latencia": 15,
    }
};*/

let datos = ["Churros", "Merinas", 200, true, null, ];
//formaPago['oculta'] = "Dame 5 centimos pa mi";
//formaPago.servidor = "https://visa.com";

//Guardar en discor el JSON en el espacio local de la pagina (20Mgb) SERIALIZADO
//window.localStorage.setItem("datos-forma-pago",JSON.stringify(formaPago,null,3));

//leer de disco el JSON y DES-SERIALIZARLO 
let formaPago = JSON.parse(window.localStorage.getItem("datos-forma-pago"));

document.write([`<br> USO DE LOS ATRIBUTOS DEL JSON MEDIANTE NOMBRE.ATRIBUTO <br><p>${formaPago.modo} - ${formaPago.clientes[3][2]}</p><br> 
FUNCIÓN JSON.STRINGFY 
<h2>${JSON.stringify(formaPago)} </h2>`]);

//window.localStorage.setItem("datos-forma-pago", JSON.stringify(formaPago, null, 3));
//JSON.stringify es convertir un objeto o estructura en memoria en un formato transmitible (para enviar por red o guardar en fichero), es SERIALIZAR, y el formato del texto puede
//ser binario, texto (XML, JSON, YAML, o uno propio), encriptado 
//let petUsu = prompt("¿Que quieres?");

let frutas = `[
    {    "nombre": "pera" , "precio": 20    },
    {    "nombre": "kiwi" , "precio": 27    },
    {    "nombre": "fresa" , "precio": 37   }]`;

//document.write(`<br> USO DE HASHMAP <br>${formaPago[petUsu]}`);

//Parsear es la forma coloquial de decir "leer o interpretar un texto" 
//puede ser para convertir un texto en otro texto, o en este caso,
//convertir un texto en un objeto en estructura en memoria. Cuando hablamos de convertir  del ultimo caso la definición técto es "DES-SERIALIZAR"
let objFrutas = JSON.parse(frutas);
document.write(` <br> USO DE LA FUNCIÓN JSON.PARSE <br>- ${objFrutas[1]["nombre"]}`);