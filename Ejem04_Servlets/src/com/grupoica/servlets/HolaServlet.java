package com.grupoica.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class HolaServlet
 */
@WebServlet("/hola")
public class HolaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public HolaServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String strNombre = request.getParameter("nombre");
		String html = "<html>" + "<head>" + "<title>Insert title here</title>" + "</head> <body>";
		if (strNombre == null || "".equals(strNombre)) {
			html += "Pon el nombre";
		} else {
			html += "<form action='./hola' method='post'>" + "<input type='submit' value='POST'/>" + ""
					+ "Veces:<input name='veces' type='number' value='Hola'/>" + "</form> " + strNombre;
		}
		html += "</body> </html>";

		// TODO Auto-generated method stub
		response.getWriter().append(html);

	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		String strNumero = request.getParameter("veces");
		String html = "<html>" + "<head>" + "<title>Insert title here</title>" + "</head> <body>";
		if (strNumero == null || "".equals(strNumero)) {
			html += "Pon un numero";
		} else {
			int numeros = Integer.parseInt(strNumero);
			if (numeros == 0) {
				html += "Debes introducir un numero mayor que 0";
			} else {
				
				//Lista
				html += "<ul>";
				for (int i = 1; i <= numeros; i++) {
					
					//Elementos de la lista
					html += "<li> - " + i + " Veces </li> ";
				}
				html+="</ul>";
			}
		}
		html += "</body> </html>";
		// TODO Auto-generated method stub
		response.getWriter().append(html);

	}

}
